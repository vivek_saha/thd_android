﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class Leveling : MonoBehaviour
{

    public int xpBase = 50;

    public int currentXP;

    int xpToNext = 0;
    int xpToCurrent = 0;

    public float percentToNext;

    //public GameObject xpMeter;
    //public GameObject xpMeter_home;
    public Slider slider;
    public Slider slider_home;

    private int oldlvl;

    //public bool first_time = true ;

    private Slot slot;
    // Use this for initialization
    //public void Start()
    //{
    //	GameObject a = GameObject.FindGameObjectWithTag("Slot");
    //	slot = a.GetComponent<Slot>();
    //	oldlvl = PlayerPrefs.GetInt("old_lvl", 1);
    //	//slot = GameObject.Find ("BeachDays").GetComponent<Slot>();
    //	if (slot.refs.credits.persistant) {
    //		// initialize the level
    //		computeLevel();
    //		// award any xp saved in player prefs
    //		AwardXp(PlayerPrefs.GetInt("CurrentXP", 0));
    //		// hack for unity bug
    //		xpMeter.SetActive(false);
    //		xpMeter.SetActive(true);
    //	}
    //}
    public void manual_start()
    {
        //GameObject a = GameObject.FindGameObjectWithTag("Slot");
        //slot = a.GetComponent<Slot>();

        //oldlvl = PlayerPrefs.GetInt("old_lvl", 1);

        //Gamemanager.Instance.level.text = PlayerPrefs.GetInt("old_lvl", 1).ToString();
        currentXP = PlayerPrefs.GetInt("CurrentXP", 0);
        //computeLevel();
        //if (first_time)
        //{
        oldlvl = GetLevel();
        PlayerPrefs.SetInt("old_lvl", oldlvl);
        //Debug.Log(oldlvl);
        //first_time = false;
        //}


        AwardXp(0);
        //slot = GameObject.Find ("BeachDays").GetComponent<Slot>();
        //if (slot.refs.credits.persistant)
        //{
        // initialize the level
        //slider.value = percentToNext;
        //Gamemanager.Instance.lbh.slider.value = percentToNext;
        // award any xp saved in player prefs
        // hack for unity bug
        //xpMeter.SetActive(false);
        //xpMeter.SetActive(true);
        //}

    }

    // Compute and update leveling info for example
    int computeLevel()
    {
        float lvl = Mathf.Sqrt(currentXP / xpBase);



        //print("lvl" + lvl);
        int clevel = Mathf.FloorToInt(lvl + 1);
        lvl_up_check(clevel);
        if (clevel == 0) clevel = 1;

        xpToNext = ((clevel) * (clevel)) * xpBase;
        xpToCurrent = ((clevel - 1) * (clevel - 1)) * xpBase;

        percentToNext = ((float)currentXP - (float)xpToCurrent) / ((float)xpToNext - (float)xpToCurrent);
        //print("percentToNext" + percentToNext);
        return clevel;
    }



    // Recompute and return current level for GUI readout
    public int GetLevel()
    {
        //Gamemanager.Instance.lbh.GetLevel();
        int clevel = 0;
        if (xpBase == 0) return clevel;

        clevel = computeLevel();
        lvl_up_check(clevel);
        return clevel;
    }

    // Award an xp amount, and also update the visual sprite meter size, and save the new XP to player prefs
    public void AwardXp(long amount)
    {
        //Gamemanager.Instance.lbh.AwardXp(amount);

        //print(" Mathf.Pow(10, (oldlvl/5)" + Mathf.Pow(10, (oldlvl / 5)));
        //chnage xp gain
        if (oldlvl < 21)
        {
            currentXP += (int)(amount / (5 + (Mathf.Pow(10, (oldlvl / 5)))));
        }
        else
        {
            currentXP += (int)(amount / (5 + (Mathf.Pow(10, 1 + (oldlvl / 5)))));
        }
        //print("amount" + amount);
        //print("currentXP" + currentXP);
        computeLevel();
        //if()
        //      {
        //if (Gamemanager.Instance.Gameplay_panel.activeSelf)
        //{
        //    if (Lvl_manager.Instance.fiveofkind_panel.activeSelf)
        //    {
        //        Invoke("wait_for_5ok", 4f);
        //    }
        //    else
        //    {
        //        if (!Lvl_manager.Instance.lvl_up_panel.activeSelf)
        //        {
        //            GetComponent<Xp_coin_effect_script>().Add_xp();
        //            //}
        //            Invoke("wait_for_effect", 1f);
        //        }
        //        else
        //        {
                   
        //            Invoke("waitfor_homepanel_open", 1f);

        //        }
                    
                
        //    }
        //    //CAll_XP_server
        //}
        //else
        //{
            //if (Gamemanager.Instance.home_panel.activeSelf)
            //{
               
            //    Invoke("waitfor_homepanel_open", 1f);
            //}
            //else
            //{

                wait_for_effect();
            //}
        //}

    }

    public void waitfor_homepanel_open()
    {
        //Gamemanager.Instance.GetComponent<Xp_coin_effect_script>().Add_xp();
        //Invoke("wait_for_effect", 1f);
    }

    public void wait_for_5ok()
    {
        //GetComponent<Xp_coin_effect_script>().Add_xp();
        //}
        Invoke("wait_for_effect", 1.5f);
    }
    public void wait_for_effect()
    {
        if (percentToNext < slider.value)
        {
            slider.DOValue(1f, 0.2f).OnComplete(() => lvl_up_check(computeLevel()));

        }
        slider.DOValue(percentToNext, 0.2f);
        slider_home.value = percentToNext;

        PlayerPrefs.SetInt("CurrentXP", currentXP);

    }

    public void lvl_up_check(int x)
    {
        //Debug.Log("lvlv_up_old_lvl" + oldlvl);
        if (GameObject.FindGameObjectWithTag("Slot") != null)
        {
            if (oldlvl < x)
            {
                //print("Level_UP");
                oldlvl = x;
                PlayerPrefs.SetInt("old_lvl", oldlvl);
                //Lvl_manager.Instance.LEvel_UP(oldlvl - 1);
                //Lvl_manager.Instance.updateUI();
            }
        }
    }

}