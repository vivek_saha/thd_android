using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pointball_script : MonoBehaviour
{

    public Coin_type ct;

    public Sprite diamond, goldcoin;

    // Start is called before the first frame update
    void Start()
    {
        if(ct== Coin_type.Diamond)
        {
            GetComponent<MeshRenderer>().material.mainTexture = diamond.texture;
        }
        else if (ct == Coin_type.GoldCoin)
        {
            GetComponent<MeshRenderer>().material.mainTexture = goldcoin.texture;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
