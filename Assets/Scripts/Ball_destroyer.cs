using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball_destroyer : MonoBehaviour
{
    public static Ball_destroyer Instance;
    public GameObject spawn_point;

    public GameObject point_ball;



    //public Coin_type ctt;
    public int nnm;


    private void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        set_pos();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ball")
        {
            ContactPoint contact = collision.contacts[0];
            getnearest_point(collision.gameObject);
            //Debug.Log(contact.point);
            Destroy(collision.gameObject);
            //foreach (Transform a in spawn_point.transform)
            //{
            //    GameObject am = Instantiate(point_ball);
            //    am.transform.position = a.position;
            //    am.transform.localScale = new Vector3(0.25f, 0.25f, 0.25f);

            //}

        }




    }


    public void spawn_points(Coin_type ss, int num_of_balls)
    {
        if (num_of_balls == 10)
        {
            foreach (Transform a in spawn_point.transform)
            {
                GameObject am = Instantiate(point_ball);
                am.transform.position = a.position;
                am.transform.localScale = new Vector3(0.25f, 0.25f, 0.25f);
                am.GetComponent<pointball_script>().ct = ss;
            }
        }
        else if (num_of_balls == 5)
        {
            int indexxx = 3;
            for (int i = 0; i < 5; i++)
            {
                GameObject ab = Instantiate(point_ball);
                ab.GetComponent<pointball_script>().ct = ss;
                ab.transform.position = spawn_point.transform.GetChild(indexxx).transform.position;
                ab.transform.localScale = new Vector3(0.25f, 0.25f, 0.25f);
                indexxx++;
            }
        }
    }

    public void set_pos()
    {
        float a = -0.374f;
        foreach (Transform m in spawn_point.transform)
        {
            m.transform.localPosition = new Vector3(a, m.transform.localPosition.y, m.transform.localPosition.z);
            a += (0.748f / 9f);
        }
    }

    public void getnearest_point(GameObject balll)
    {
        Vector3 pos = balll.transform.position;
        float npoint = 1000f;
        int indexxx = 0;
        int num_point = 0;

        List<GameObject> lst = new List<GameObject>();
        foreach (Transform a in spawn_point.transform)
        {
            lst.Add(a.gameObject);

        }
        foreach (GameObject a in lst)
        {
            float dist = Vector3.Distance(pos, a.transform.position);
            if (dist < npoint)
            {
                npoint = dist;
                indexxx = lst.IndexOf(a);
            }

        }
        switch (balll.GetComponent<Ball_script>().brt)
        {
            case ball_shot_type.Miss:

                num_point = 1;

                break;
            case ball_shot_type.Good:
                if (balll.GetComponent<Ball_script>().goal)
                {
                    num_point = 3;
                    if (indexxx == 0)
                    {
                        indexxx = 0;
                    }
                    else if (indexxx == lst.Count - 1)
                    {
                        indexxx = 6;
                    }
                    else
                    {
                        indexxx -= 1;
                    }
                }
                else
                {
                    //miss logic
                    num_point = 1;

                }
                break;
            case ball_shot_type.Perfect:
                num_point = 5;
                if (indexxx <= 2)
                {
                    indexxx = 0;
                }
                else if (indexxx >= lst.Count - 2)
                {
                    indexxx = 4;
                }
                else
                {
                    indexxx -= 2;
                }
                break;
            default:
                break;
        }



        for (int i = 0; i < num_point; i++)
        {
            GameObject ab = Instantiate(point_ball);
            ab.GetComponent<pointball_script>().ct = balll.GetComponent<Ball_script>().ct;
            ab.transform.position = lst[indexxx].transform.position;
            ab.transform.localScale = new Vector3(0.25f, 0.25f, 0.25f);
            indexxx++;
        }



    }


    public void spin_win_call( int num)
    {
        //ctt = dd;
        nnm = num;
        //Debug.Log(num);
        StartCoroutine("Spin_win");
    }

    IEnumerator Spin_win()
    {
        switch (nnm)
        {
           
            case 1:
                spawn_points(Coin_type.Diamond, 10);
                yield return new WaitForSeconds(0.5f);
                spawn_points(Coin_type.Diamond, 10);
                yield return new WaitForSeconds(0.5f);
                spawn_points(Coin_type.Diamond, 5);
                break;
            case 2:
                spawn_points(Coin_type.Diamond, 10);
                yield return new WaitForSeconds(0.5f);
                spawn_points(Coin_type.Diamond, 10);
                yield return new WaitForSeconds(0.5f);
                spawn_points(Coin_type.Diamond, 10);
                yield return new WaitForSeconds(0.5f);
                spawn_points(Coin_type.Diamond, 10);
                yield return new WaitForSeconds(0.5f);
                spawn_points(Coin_type.Diamond, 10);
                break;
            case 3:
                spawn_points(Coin_type.Diamond, 10);
                yield return new WaitForSeconds(0.5f);
                spawn_points(Coin_type.Diamond, 10);
                yield return new WaitForSeconds(0.5f);
                spawn_points(Coin_type.Diamond, 10);
                yield return new WaitForSeconds(0.5f);
                spawn_points(Coin_type.Diamond, 10);
                yield return new WaitForSeconds(0.5f);
                spawn_points(Coin_type.Diamond, 10);

                yield return new WaitForSeconds(1f);

                spawn_points(Coin_type.Diamond, 10);
                yield return new WaitForSeconds(0.5f);
                spawn_points(Coin_type.Diamond, 10);
                yield return new WaitForSeconds(0.5f);
                spawn_points(Coin_type.Diamond, 10);
                yield return new WaitForSeconds(0.5f);
                spawn_points(Coin_type.Diamond, 10);
                yield return new WaitForSeconds(0.5f);
                spawn_points(Coin_type.Diamond, 10);

                break;
            case 4:

                spawn_points(Coin_type.GoldCoin, 10);
                yield return new WaitForSeconds(0.5f);
                spawn_points(Coin_type.GoldCoin, 10);
                yield return new WaitForSeconds(0.5f);
                spawn_points(Coin_type.GoldCoin, 5);
                break;
            case 5:
                spawn_points(Coin_type.GoldCoin, 10);
                yield return new WaitForSeconds(0.5f);
                spawn_points(Coin_type.GoldCoin, 10);
                yield return new WaitForSeconds(0.5f);
                spawn_points(Coin_type.GoldCoin, 10);
                yield return new WaitForSeconds(0.5f);
                spawn_points(Coin_type.GoldCoin, 10);
                yield return new WaitForSeconds(0.5f);
                spawn_points(Coin_type.GoldCoin, 10);
                break;
            case 6:
                spawn_points(Coin_type.GoldCoin, 10);
                yield return new WaitForSeconds(0.5f);
                spawn_points(Coin_type.GoldCoin, 10);
                yield return new WaitForSeconds(0.5f);
                spawn_points(Coin_type.GoldCoin, 10);
                yield return new WaitForSeconds(0.5f);
                spawn_points(Coin_type.GoldCoin, 10);
                yield return new WaitForSeconds(0.5f);
                spawn_points(Coin_type.GoldCoin, 10);

                yield return new WaitForSeconds(1f);

                spawn_points(Coin_type.GoldCoin, 10);
                yield return new WaitForSeconds(0.5f);
                spawn_points(Coin_type.GoldCoin, 10);
                yield return new WaitForSeconds(0.5f);
                spawn_points(Coin_type.GoldCoin, 10);
                yield return new WaitForSeconds(0.5f);
                spawn_points(Coin_type.GoldCoin, 10);
                yield return new WaitForSeconds(0.5f);
                spawn_points(Coin_type.GoldCoin, 10);
                break;
            case 7:
                //fragment reward
                break;
            default:
                break;
        }
    }

}
