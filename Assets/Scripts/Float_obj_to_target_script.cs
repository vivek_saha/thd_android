using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Float_obj_to_target_script : MonoBehaviour
{
    public Sprite diam, gold;
    public Image coin_img;
    public Coin_type coin_type;

    public Vector3 targetPos;

    // Start is called before the first frame update
    void Start()
    {
        switch (coin_type)
        {
            case Coin_type.Diamond:
                coin_img.sprite = diam;
                break;
            case Coin_type.GoldCoin:
                coin_img.sprite = gold;
                break;
            default:
                coin_img.sprite = diam;
                break;
        }
        //StartCoroutine("abc");
        Invoke("wait_for_anim", 1f);
        //wait_for_anim();
    }

    private void wait_for_anim()
    {
        transform.DOMove(targetPos, 0.5f).OnComplete(()=> { Destroy(this.gameObject); });
    }

    // Update is called once per frame
    void Update()
    {

    }
    IEnumerator abc()
    {
        AnimationCurve curveX = new AnimationCurve(new Keyframe[]
        {
            new Keyframe(0f, base.transform.position.x),
            new Keyframe(0.5f, this.targetPos.x)
        });
        AnimationCurve curveY = new AnimationCurve(new Keyframe[]
        {
            new Keyframe(0f, base.transform.position.y),
            new Keyframe(0.5f, this.targetPos.y)
        });
        curveY.AddKey(0.2f, base.transform.position.y - 4f);
        float startTime = Time.time;
        Vector3 startPos = base.transform.position;
        float speed = UnityEngine.Random.Range(0.4f, 0.6f);
        float distCovered = 0f;
        while (distCovered < 1f)
        {
            distCovered = (Time.time - startTime) * speed;
            base.transform.position = new Vector3(curveX.Evaluate(distCovered), curveY.Evaluate(distCovered), 0f);
            yield return new WaitForEndOfFrame();
        }
        UnityEngine.Object.Destroy(this.gameObject);
        yield break;
    }
}
