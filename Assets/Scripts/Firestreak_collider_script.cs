using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public enum firestreak_num { c1, c2, c3, c4, c5, c6, c7 }
public class Firestreak_collider_script : MonoBehaviour
{

    public firestreak_num fn;
    public int[] score_array = new int[] { 2, 5, 10, 20, 50, 100, 150, 200 };
    public int score_value;
    public TextMeshPro score_text;
    public MeshRenderer flame;

    public bool fire = false;


    // Start is called before the first frame update
    void Start()
    {

        score_value = score_array[Random.Range(0, score_array.Length-5)];
        score_text.text = "x"+score_value.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "PointBall")
        {
            //Debug.Log(fn);

            Floating_text_controller.Instance.spawn_text(collision.gameObject.GetComponent<pointball_script>().ct,collision.transform,score_value.ToString());
            Destroy(collision.gameObject);
            int rand = Random.Range(0, 1) > 0.5f ? 8 : 5;
            score_value = score_array[Random.Range(0, rand)];
            score_text.text = "x" + score_value.ToString();
            if (!fire)
            {
                fire = true;
                Color cl = flame.material.color;
                cl.a = 1;
                flame.material.color = cl;
                Spin_controller.Instance.Addflame();
            }
        }
    }
}
