using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum Coin_type { Diamond, GoldCoin }
public class CoinManager : MonoBehaviour
{
    public static CoinManager instance;

    public Transform canvas_for_float_coin;

    public GameObject coin_float;
    private int gold_coin;
    public int GoldCoin
    {
        get
        {
            gold_coin = PlayerPrefs.GetInt("GoldCoin", 0);
            return gold_coin;
        }
        set
        {
            gold_coin = value;
            PlayerPrefs.SetInt("GoldCoin", value);
        }
    }


    private float diamond_;
    public float Diamond
    {
        get
        {
            diamond_ = PlayerPrefs.GetFloat("Diamond", 0);
            return diamond_;
        }
        set
        {
            diamond_ = value;
            PlayerPrefs.SetFloat("Diamond", value);
        }
    }

    public Transform coin_top_pos, diam_top_pos;

    public void AddDiamond(float count)
    {
        Diamond += count;
    }

    public void AddGoldCoin(int count)
    {
        GoldCoin += count;
    }




    private void Awake()
    {
        instance = this;
    }




    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public float Random_num_diamond()
    {
        float num;
        num = 9;
        //if (Diamond < Setting_API.Instance.diamond_limit / 5)
        //{
        //    num = Random.Range(1000.0f, 2000.0f) / 7;

        //}
        //else if (Diamond >= Setting_API.Instance.diamond_limit / 5 && Diamond < Setting_API.Instance.diamond_limit / 2)
        //{
        //    num = Random.Range(500.0f, 1000.0f) / 7;
        //}
        //else if (Diamond >= Setting_API.Instance.diamond_limit / 2 && Diamond < 4 * (Setting_API.Instance.diamond_limit / 5))
        //{
        //    num = Random.Range(300.0f, 500.0f) / 7;
        //}
        //else if (Diamond >= 4 * (Setting_API.Instance.diamond_limit / 5) && Diamond < (Setting_API.Instance.diamond_limit - (4 * (Setting_API.Instance.diamond_limit / 10))))
        //{
        //    num = Random.Range(100.0f, 300.0f) / 7;
        //}
        //else if (Diamond >= (Setting_API.Instance.diamond_limit - (4 * (Setting_API.Instance.diamond_limit / 10))) && Diamond < (Setting_API.Instance.diamond_limit - (4 * (Setting_API.Instance.diamond_limit / 20))))
        //{
        //    num = Random.Range(50.0f, 100.0f) / 7;
        //}
        //else if (Diamond >= (Setting_API.Instance.diamond_limit - (4 * (Setting_API.Instance.diamond_limit / 20))) && Diamond < Setting_API.Instance.diamond_limit)
        //{
        //    num = Random.Range(1.0f, 10.0f) / 7;
        //}
        //else
        //{
        //    num = 0.1f;
        //}
        num = Mathf.Round(num * 10.0f) * 0.1f;
        return num;
    }

    public void Collect_diamond(Vector3 ddidid)
    {
        int axsc = Random.Range(5, 10);
        for (int i = 0; i < axsc; i++)
        {

            GameObject adff = Instantiate(coin_float, ddidid, Quaternion.identity);
            adff.transform.SetParent(canvas_for_float_coin);
            Vector3 rannnx = Random.insideUnitCircle / 20;
            adff.transform.SetPositionAndRotation(ddidid + rannnx, Quaternion.identity);
            adff.transform.localScale = Vector3.one;
            adff.GetComponent<Float_obj_to_target_script>().coin_type = Coin_type.Diamond;
            adff.GetComponent<Float_obj_to_target_script>().targetPos = diam_top_pos.position;
        }
    }
    public void Collect_Gold_coin(Vector3 asdf)
    {
        int axs = Random.Range(5, 10);
        for (int i = 0; i < axs; i++)
        {

            GameObject a = Instantiate(coin_float, asdf, Quaternion.identity);
            a.transform.SetParent(canvas_for_float_coin);
            Vector3 rannn = Random.insideUnitCircle / 20;
            a.transform.SetPositionAndRotation(asdf + rannn, Quaternion.identity);
            a.transform.localScale = Vector3.one;
            a.GetComponent<Float_obj_to_target_script>().coin_type = Coin_type.GoldCoin;
            a.GetComponent<Float_obj_to_target_script>().targetPos = coin_top_pos.position;
        }
    }
}

public static class AbbrevationUtility
{
    private static readonly SortedDictionary<long, string> abbrevations = new SortedDictionary<long, string>
     {
         {1000,"K"},
         {1000000, "M" },
         {1000000000, "B" },
         {1000000000000,"T"}
     };

    public static string AbbreviateNumber(float number)
    {
        for (int i = abbrevations.Count - 1; i >= 0; i--)
        {
            KeyValuePair<long, string> pair = abbrevations.ElementAt(i);
            if (Mathf.Abs(number) >= pair.Key)
            {
                float roundedNumber = (number / pair.Key);
                roundedNumber = (float)System.Math.Round(roundedNumber, 2);
                return roundedNumber.ToString() + pair.Value;
            }
        }
        return number.ToString();
    }
}
public static class AbbrevationUtility_LeaderBoard
{
    private static readonly SortedDictionary<long, string> abbrevations = new SortedDictionary<long, string>
     {
         {1000,"K"},
         {1000000, "M" },
         {1000000000, "B" },
         {1000000000000,"T"}
     };

    public static string AbbreviateNumber(float number)
    {
        for (int i = abbrevations.Count - 1; i >= 0; i--)
        {
            KeyValuePair<long, string> pair = abbrevations.ElementAt(i);
            if (Mathf.Abs(number) >= pair.Key)
            {
                float roundedNumber = (number / pair.Key);
                roundedNumber = (float)System.Math.Round(roundedNumber, 2);
                return roundedNumber.ToString() + pair.Value;
            }
        }
        return number.ToString();
    }
}
