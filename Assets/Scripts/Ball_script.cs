using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball_script : MonoBehaviour
{

    public GameObject target;
    public float angle;


    public bool goal = false;
    public ball_shot_type brt = ball_shot_type.Miss;

    public Coin_type ct;
    public Vector3 temp_pos;
    public Texture diamond, gold_texture;

    // Start is called before the first frame update
    void Start()
    {
        if(ct== Coin_type.Diamond)
        {
            GetComponent<MeshRenderer>().material.mainTexture = diamond;
        }
        else if (ct == Coin_type.GoldCoin)
        {
            GetComponent<MeshRenderer>().material.mainTexture = gold_texture;
        }
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(this.gameObject.transform.position);
        if (temp_pos != null)
        {
            Debug.DrawLine(this.transform.position, temp_pos, Color.cyan);
        }
        Vector3 mk = calcBallisticVelocityVector(gameObject.transform.position, target.transform.position, angle);
        PlotTrajectory(transform.position, mk, 0.1f, 2f);
    }   
    
    public void shoot_ball(Vector3 adc)
    {
        temp_pos = adc;
        if(Random.Range(0.0f,1.0f)>0.7f)
        {
            Vector2 aa = Random.insideUnitCircle;
            adc += new Vector3(aa.x,0,0);
            //Debug.Log(adc);

        }
        //Debug.Log(target.transform.parent.parent.parent.transform.localPosition);
        //temp_pos = target.transform.position;
        //OnDrawGizmos();
        Vector3 mk = calcBallisticVelocityVector(this.gameObject.transform.position, adc, angle);
        //if(mk == nan)
        if (!float.IsNaN(mk.x) && !float.IsNaN(mk.y) && !float.IsNaN(mk.z))
        {
            //Do stuff
        //Debug.Log(adc); 
            this.GetComponent<Rigidbody>().isKinematic = false;
            //this.GetComponent<Rigidbody>().
            //this.GetComponent<Rigidbody>().velocity = Vector3.zero;
            this.GetComponent<Rigidbody>().AddForce(mk, ForceMode.Impulse);
        }
        //PlotTrajectory(transform.position, mk, 1f, 2f);

    }

    public void PlotTrajectory(Vector3 start, Vector3 startVelocity, float timestep, float maxTime)
    {
        Vector3 prev = start;
        for (int i = 1; ; i++)
        {
            float t = timestep * i;
            if (t > maxTime) break;
            Vector3 pos = PlotTrajectoryAtTime(start, startVelocity, t);
            if (Physics.Linecast(prev, pos)) break;
            Debug.DrawLine(prev, pos, Color.green);

            prev = pos;
        }
        //temp_pos = prev;
        //OnDrawGizmos();
    }





    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(temp_pos, 1f);
    }
    public Vector3 PlotTrajectoryAtTime(Vector3 start, Vector3 startVelocity, float time)
    {
        return start + startVelocity * time + Physics.gravity * time * time * 0.5f;
    }

    Vector3 calcBallisticVelocityVector(Vector3 source, Vector3 targetdd, float angle)
    {
        Vector3 direction = targetdd - source;
        float h = direction.y;
        direction.y = 0;
        float distance = direction.magnitude;
        float a = angle * Mathf.Deg2Rad;
        direction.y = distance * Mathf.Tan(a);
        distance += h / Mathf.Tan(a);

        
        // calculate velocity
        float velocity = Mathf.Sqrt(distance * Physics.gravity.magnitude / Mathf.Sin(2 * a));
        return velocity * direction.normalized;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.name == "Basket_ring")
        {
            brt = ball_shot_type.Good;
        }
    }
}