using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

using TMPro;
using UnityEngine.UI;
using System;

public enum ball_shot_type { Miss, Good, Perfect }
public class Balll_Controller : MonoBehaviour
{
    public Vector3 origin_pos_ball;
    public GameObject Ball;
    public GameObject target_basket;
    GameObject bl;

    public int initial_ball;

    public GameObject clear_bar;

    public Text timer_text,ball_text;

    bool enable_timer = false;
    public float _timer;
    public float timer
    {
        get
        {
            return PlayerPrefs.GetFloat("_timer", 0);
        }
        set
        {
            timer_text.text = "Next in:" + ((int)value).ToString() + "s";
            _timer = value;
            PlayerPrefs.SetFloat("_timer", _timer);
        }
    }

    public int ball_counter
    {
        get
        {
            return PlayerPrefs.GetInt("ball_counter", initial_ball);
        }
        set
        {
            ball_text.text = "Ball:" + value.ToString();
            PlayerPrefs.SetInt("ball_counter", value);
            if (value < initial_ball)
            {
                //start_timer();
                if (!enable_timer)
                {
                    //timer = 400;
                    enable_timer = true;
                }
            }
            else
            {
                //disable_timer();
                enable_timer = false;
            }
        }
    }


    public List<GameObject> basket_net_inc;
    //normal x = -0.01709697f
    //wide x = -0.0204f


    public List<TextMeshPro> multiplier;


    // Start is called before the first frame update
    void Start()
    {
        ball_counter= PlayerPrefs.GetInt("ball_counter", initial_ball);
        onstart();
        Generate_ball();
    }

    public void onstart()
    {
        long nowDate = DateTime.UtcNow.Ticks / 10000 / 1000;
        long lastDate = long.Parse(PlayerPrefs.GetString("lastDate", nowDate.ToString()));
        //Debug.Log("lastDate =>" + lastDate);
        long secondsSinceLastDate = nowDate - lastDate;
        //Debug.Log("secondsSinceLastDate =>" + secondsSinceLastDate);
        //value__1 = PlayerPrefs.GetInt("value__1", (int)secondsSinceLastDate);
        //PlayerPrefs.SetInt()
        minus_time((int)secondsSinceLastDate);

    }

    void minus_time(int x)
    {
        if (x > 300)
        {
            int part = x / 300;
            for (int i = 0; i < part; i++)
            {
                ball_counter++;
            }
            if (x - (300 * part) > timer)
            {
                ball_counter++;
                timer = 300 - (x - (300 * part) - timer);
            }
            else
            {
                timer -= x - (300 * part);
            }
        }
        else
        {
            if (x > timer)
            {
                ball_counter++;
                timer = 300 - (x - timer);
            }
            else
            {
                timer -= x;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (enable_timer)
        {
            if (timer <= 0)
            {
                if (ball_counter < initial_ball)
                {
                    ball_counter++;
                    timer = 300;
                }
                else
                {
                    enable_timer = false;
                }
            }
            else
            {
                timer -= Time.deltaTime;
            }
        }
        else
        {
            timer_text.text = "";
        }
        if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
        {
            //Generate_ball();


        }
        else if (Input.GetMouseButtonUp(0) && !EventSystem.current.IsPointerOverGameObject() && bl != null)
        {
            ball_counter--;
            bl.GetComponent<Ball_script>().shoot_ball(target_basket.transform.position);
            bl = null;
            //Generate_ball();
            StartCoroutine("delay_gen_ball");
        }
    }

    IEnumerator delay_gen_ball()
    {
        yield return new WaitForSeconds(0.5f);
        Generate_ball();
    }

    private void Generate_ball()
    {
        bl = null;
        bl = Instantiate(Ball);
        bl.transform.localScale = Vector3.one;
        bl.transform.position = origin_pos_ball;
        bl.GetComponent<Ball_script>().target = target_basket;
        bl.GetComponent<Ball_script>().ct = UnityEngine.Random.Range(0.0f, 1.0f) > 0.8f ? Coin_type.Diamond : Coin_type.GoldCoin;



    }

    public void Disable_clear_bar()
    {
        clear_bar.SetActive(false);
        StartCoroutine("enable_clear_bar");
    }


    IEnumerator enable_clear_bar()
    {
        yield return new WaitForSeconds(2f);
        clear_bar.SetActive(true);
    }

    IEnumerator enable_wide_ring()
    {
        foreach (GameObject os in basket_net_inc)
        {
            os.transform.localScale = new Vector3(1.2f, 1f, 1f);
            Vector3 m = os.transform.localPosition;
            m.x = -0.0204f;
            os.transform.localPosition = m;
        }


        yield return new WaitForSeconds(10f);
        Disable_wide_ring();
    }

    public void Disable_wide_ring()
    {
        foreach (GameObject os in basket_net_inc)
        {
            os.transform.localScale = Vector3.one;
            Vector3 m = os.transform.localPosition;
            m.x = -0.01709697f;
            os.transform.localPosition = m;
        }
    }
}
